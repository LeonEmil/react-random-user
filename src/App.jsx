import React, { useState, useEffect } from 'react'
import Loader from 'react-loader-spinner'
import axios from 'axios'
import './App.css';

function App() {

  // Initial variables
  const [users, setUsers] = useState([])
  const [searchUser, setSearchUser] = useState("")
  const [displayConfig, setDisplayConfig] = useState(false)
  const [configUser, setConfigUser] = useState({
    firstName: "",
    lastName: "",
    image: "",
    email: "",
    phone: "",
    city: "",
    state: "",
    key: 0
  })

  // Get users
  useEffect(() => {
      axios.get("https://randomuser.me/api/?results=90").then(response => {
        setUsers(response.data.results)
      })
  }, [])

  // Sort users
  const [sortBy, setSortBy] = useState("")

  const sortUsers = (sortBy) => {
    if(sortBy === "") return users
    if(sortBy === "name") return users.sort((a, b) => `${a.name.first} ${a.name.last}` > `${b.name.first} ${b.name.last}` ? 1 : -1)
    if(sortBy === "email") return users.sort((a, b) => a.email > b.email ? 1 : -1)
    if(sortBy === "phone") return users.sort((a, b) => a.phone > b.phone ? 1 : -1)
    if(sortBy === "location") return users.sort((a, b) => `${a.location.city} ${a.location.state}` > `${b.location.city} ${b.location.state}` ? 1 : -1)
  }

  // Filter users
  const filterUsers = sortUsers(sortBy).filter(user => {
    return searchUser !== "" ? `${user.name.first} ${user.name.last}`.toLowerCase().startsWith(searchUser.toLowerCase()) : user
  })

  // Config users
  const handleConfig = (user, key) => {
    setDisplayConfig(true)
    setConfigUser({
      firstName: user.name.first,
      lastName: user.name.last,
      image: user.picture.thumbnail,
      email: user.email,
      phone: user.phone,
      city: user.location.city,
      state: user.location.state,
      key: key
    })
  }

  // Save user changes
  const handleSubmit = (e) => {
    e.preventDefault()
    let updatedUsers = users
    updatedUsers[configUser.key].name.first = configUser.firstName
    updatedUsers[configUser.key].name.last = configUser.lastName
    updatedUsers[configUser.key].picture.large = configUser.image
    updatedUsers[configUser.key].email = configUser.email
    updatedUsers[configUser.key].phone = configUser.phone
    updatedUsers[configUser.key].location.city = configUser.city
    updatedUsers[configUser.key].location.state = configUser.state
    setUsers(updatedUsers)
    setDisplayConfig(false)
  }

  return (
    <>
    {
      displayConfig ?
      <div className="config-container">
        <div className="config">
          <div className="config__exit" onClick={() => {setDisplayConfig(false)}}></div>
          <form className="config__form" onSubmit={(e) => {handleSubmit(e)}}>
            <label htmlFor="first-name" className="config__label">First name</label>
            <input type="text" name="first-name" className="config__input" value={configUser.firstName} onChange={(e) => {setConfigUser({...configUser, firstName: e.target.value})}}/>
            <label htmlFor="last-name" className="config__label">Last name</label>
            <input type="text" name="last-name" className="config__input" value={configUser.lastName} onChange={(e) => {setConfigUser({...configUser, lastName: e.target.value})}}/>
            <label htmlFor="image" className="config__label">Image</label>
            <input type="file" name="image" className="config__input" onChange={(e) => {setConfigUser({...configUser, image: URL.createObjectURL(e.target.files[0])}); console.log(URL.createObjectURL(e.target.files[0]))}}/>
            <label htmlFor="email" className="config__label">Email</label>
            <input type="text" name="email" className="config__input" value={configUser.email} onChange={(e) => {setConfigUser({...configUser, email: e.target.value})}}/>
            <label htmlFor="phone" className="config__label">Phone</label>
            <input type="text" name="phone" className="config__input" value={configUser.phone} onChange={(e) => {setConfigUser({...configUser, phone: e.target.value})}}/>
            <label htmlFor="city" className="config__label">City</label>
            <input type="text" name="city" className="config__input" value={configUser.city} onChange={(e) => {setConfigUser({...configUser, city: e.target.value})}}/>
            <label htmlFor="state" className="config__label">State</label>
            <input type="text" name="state" className="config__input" value={configUser.state} onChange={(e) => {setConfigUser({...configUser, state: e.target.value})}}/>
            <input type="submit" value="Save"/>
          </form>
        </div>
      </div>
      :
      null
    }
    <h1>Random Users</h1>
    <form className="form">
      <label htmlFor="input" className="form__label">Search user: </label>
      <input type="text" placeholder="Type a name..." className="form__input" id="input" onChange={(e) => {setSearchUser(e.target.value)}} />
    </form>
    <label htmlFor="select">Sort by: </label>
    <select name="select" id="select" onChange={(e) => {setSortBy(e.target.value)}}>
      <option value="">Choise an option</option>
      <option value="name">Name</option>
      <option value="email">Email</option>
      <option value="phone">Phone</option>
      <option value="location">Location</option>
    </select>
    <div className="user-grid">
      {
        users.length === 0 ?
          <Loader
            type="TailSpin"
            color="#00BFFF"
            height={100}
            width={100}
            timeout={3000} //3 secs
            className="loader-spinner"
          />
        : users.length > 0 && filterUsers.length === 0 ?
            <h2>User not found</h2> 
        : 
        users.length > 0 && filterUsers.length > 0 ?
            filterUsers.map((user, key) => (
              <div className="user-card" key={key}>
                <div className="user-card__icon" onClick={() => {handleConfig(user, key)}}></div>
                <h2 className="user-card__name">{`${user.name.first} ${user.name.last}`}</h2>
                <img src={user.picture.large} alt={`${user.name.first} ${user.name.last}`} className="user-card__img"/>
                <p className="user-card__email">{user.email}</p>
                <p className="user-card__phone">{user.phone}</p>
                <p className="user-card__location">{`${user.location.city}, ${user.location.state}`}</p>
              </div>
            ))
        : null
      }
    </div>
    </>
  );
}

export default App;
